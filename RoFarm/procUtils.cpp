#include "stdafx.h"
#include "procUtils.h"

namespace procUtils
{
	void getProcIdsByName(const tstring& procName, std::vector<DWORD>& idProcs, DWORD flags)
	{
		PROCESSENTRY32 entry;
		entry.dwSize = sizeof(PROCESSENTRY32);

		HANDLE snapshot = CreateToolhelp32Snapshot(flags, NULL);

		if (Process32First(snapshot, &entry) == TRUE)
		{
			while (Process32Next(snapshot, &entry) == TRUE)
			{
                std::wstring strExe(entry.szExeFile);
				if (strExe == procName)
				{
					//HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, entry.th32ProcessID);

					// Do stuff..
					//TerminateProcess(hProcess, 0);
					idProcs.push_back(entry.th32ProcessID);

					//CloseHandle(hProcess);
				}
			}
		}
		CloseHandle(snapshot);
	}
}
