#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <QtCore>
#include <QtXml>
#include "wndUtils.h"
#include <QDebug>

namespace ro {

const QString commands_file = "commands.xml";
const QString accounts_file = "logins.xml";
const QString auth_file = "auth.xml";

struct roCommand
{
    QString cmd;
    int delay;
    int repeat;
};

struct roAccount
{
    roAccount(QString log, QString pass)
        : login(log), password(pass)
    {        
        login_cmd.delay = 100;
        password_cmd.delay = 100;
        login_cmd.repeat = 1;
        password_cmd.repeat = 1;
        login_cmd.cmd.append("\"");
        login_cmd.cmd.append(log);
        login_cmd.cmd.append("\"");
        password_cmd.cmd.append("\"");
        password_cmd.cmd.append(pass);
        password_cmd.cmd.append("\"");
    }

    QString login;
    QString password;
    roCommand login_cmd;
    roCommand password_cmd;
};

QList<roCommand> command_list(const QString cmd_file_path);
QList<roAccount> account_list();
void add_account(ro::roAccount account);
void SendMsg(HWND hWnd, QString cmd, int delay);
void SendSeqMsg(HWND hWnd, ro::roCommand& cmd);

}

#endif // FILEUTILS_H
