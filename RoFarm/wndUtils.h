#ifndef WNDUTILS_H
#define WNDUTILS_H

#include "stdafx.h"
namespace wndUtils
{
	bool getClassNameByHWND(HWND hWnd, tstring& className);

    BOOL GetWindowStatus(HWND hWnd, int& status);

    int MinimizeWindow(HWND hWnd);
    int RestoreWindow(HWND hWnd);

	HWND FindWindowFromProcessId(DWORD dwProcessId);
    HWND FindWindowFromProcess(HANDLE hProcess);
}

#endif // WNDUTILS_H
