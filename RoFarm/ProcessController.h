#ifndef PROCESSCONTROLLER_H
#define PROCESSCONTROLLER_H

#include <QObject>
#include <QThread>
#include <QSharedPointer>
#include "ProcessWork.h"

namespace ro {

class ProcessController : public QObject
{
    Q_OBJECT
public:
    ProcessController(roAccount account, QObject *parent = 0);
    ProcessController(HWND pid, roAccount account, QObject *parent = 0);
    virtual ~ProcessController();
    ProcessWork *const process() { return thread_.data(); }

    bool Run();
    bool Stop();
    bool Pause();

signals:
    void finished();
    void stoped();
    void paused();
    void err();

private:
    HWND pid_;
    QSharedPointer<ProcessWork> thread_;
};
}

#endif // PROCESSCONTROLLER_H
