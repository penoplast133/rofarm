#include "ProcessManager.h"

ro::ProcessManager::ProcessManager(QWidget *parent)
    : QWidget(parent),
      main_layout_(new QVBoxLayout())
{
    setWindowTitle("RO Farm Control Panel");

    QList<roAccount> acc_list = account_list();
    ProcessWidget *proc_wgt = 0;
    for (int i = 0; i < acc_list.size(); ++i) {
        proc_wgt = new ProcessWidget(acc_list[i]); //allocation
        wgt_ptr_list_.append(proc_wgt);
        main_layout_->addWidget(proc_wgt);
    }

    setLayout(main_layout_.data());
}

ro::ProcessManager::ProcessManager(const QList<HWND>& hwnd_list, QWidget *parent)
    : QWidget(parent), hwnd_list_(hwnd_list),
      main_layout_(new QVBoxLayout())
{
    setWindowTitle("RO Farm Control Panel");

    init_mass_ctrl_panel();

    QList<roAccount> acc_list = account_list();
    ProcessWidget *proc_wgt = 0;
    for (int i = 0; i < hwnd_list_.size(); ++i) {
        proc_wgt = new ProcessWidget(hwnd_list_[i], acc_list[i]); //allocation
        QObject::connect(proc_wgt, SIGNAL(status_changed()),this, SLOT(mass_finish()));
        wgt_ptr_list_.append(proc_wgt);
        main_layout_->addWidget(proc_wgt);
    }

    setLayout(main_layout_.data());
}

ro::ProcessManager::~ProcessManager() {
    for (int i = 0; i < wgt_ptr_list_.size(); ++i) {
        delete (wgt_ptr_list_[i]);
    }
}

void ro::ProcessManager::mass_stop() {
    enable_input();
    for (int i = 0; i < wgt_ptr_list_.size(); ++i) {
        wgt_ptr_list_[i]->stop_clicked();
    }
}

void ro::ProcessManager::mass_run() {
    disable_input();
    for (int i = 0; i < wgt_ptr_list_.size(); ++i) {
        wgt_ptr_list_[i]->set_work_time(work_le->text());
        wgt_ptr_list_[i]->set_tp_time(tp_le->text());
        wgt_ptr_list_[i]->start_clicked();
    }
}

void ro::ProcessManager::mass_finish() {
    enable_input();
}

void ro::ProcessManager::disable_input() {
    tp_le->setDisabled(true);
    work_le->setDisabled(true);
    run_bttn->setDisabled(true);
}

void ro::ProcessManager::enable_input() {
    tp_le->setEnabled(true);
    work_le->setEnabled(true);
    run_bttn->setEnabled(true);
}

void ro::ProcessManager::init_mass_ctrl_panel() {
    QWidget *mcw = new QWidget();
    QLabel *tp_lbl = new QLabel("tp time");
    QLabel *work_lbl = new QLabel("work time");
    tp_le = new QLineEdit("5000");
    work_le = new QLineEdit("600");
    run_bttn = new QPushButton;
    stop_bttn = new QPushButton;
    QVBoxLayout *mlayout = new QVBoxLayout;
    QHBoxLayout *hlayout = new QHBoxLayout;

    tp_le->setMaximumWidth(75);
    work_le->setMaximumWidth(75);
    run_bttn->setMaximumWidth(25);
    stop_bttn->setMaximumWidth(25);
    run_bttn->setIcon(QIcon("://run.png"));
    stop_bttn->setIcon(QIcon("://stop.png"));

    hlayout->addWidget(tp_lbl);
    hlayout->addWidget(tp_le);
    hlayout->addSpacing(40);
    hlayout->addWidget(work_lbl);
    hlayout->addWidget(work_le);
    hlayout->addSpacing(70);
    hlayout->addWidget(run_bttn);
    hlayout->addWidget(stop_bttn);

    mlayout->addWidget(new QLabel("<b>MASS CONTROL</b>"), 0, Qt::AlignHCenter);
    mlayout->addLayout(hlayout);

    mcw->setLayout(mlayout);
    main_layout_->addWidget(mcw);
    main_layout_->addSpacing(30);

    QObject::connect(run_bttn, SIGNAL(clicked()),
                     this, SLOT(mass_run()));
    QObject::connect(stop_bttn, SIGNAL(clicked()),
                     this, SLOT(mass_stop()));
}
