#include <QApplication>
#include <QString>
#include <QProcess>
#include <QDebug>
#include "ProcessWidget.h"
#include "ProcessManager.h"
#include "ProcessWork.h"
#include "wndUtils.h"
#include "procUtils.h"
#include "roUtils.h"
#include "stdafx.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);    

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    HWND mainwnd;

    WCHAR file_path[] = L"C:\\IRR\\ro.bin";
    const char file_path2[] = "C:\\IRR\\ro.bin";
    QString file_path3 = "C:\\IRR\\ro.bin";
    QStringList arg_list;


    int wnd_count = ro::account_list().size();
    for (int i = 0; i < wnd_count; ++i) {
        ZeroMemory( &si, sizeof(si) );
        si.cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );
        if( !CreateProcess( file_path,   // No module name (use command line)
                            NULL,        // Command line
                            NULL,           // Process handle not inheritable
                            NULL,           // Thread handle not inheritable
                            FALSE,          // Set handle inheritance to FALSE
                            0,              // No creation flags
                            NULL,           // Use parent's environment block
                            NULL,           // Use parent's starting directory
                            &si,            // Pointer to STARTUPINFO structure
                            &pi )           // Pointer to PROCESS_INFORMATION structure
                )
        {
            qDebug() << "CreateProcess failed: " << GetLastError();
        }
    }
    Sleep((wnd_count*1000) + 5000);

    tstring procName = L"ro.bin";
    std::vector<DWORD> ids;
    QList<HWND> hwnd_list;

    procUtils::getProcIdsByName(procName, ids);

    HWND hWnd = 0;
    for (int i = 0; i < ids.size(); ++i) {
        hWnd = wndUtils::FindWindowFromProcessId(ids[i]);
        if (hWnd) {
            hwnd_list.append(hWnd);
        }
    }

    ro::ProcessManager w(hwnd_list);
    w.show();

    return a.exec();
}
