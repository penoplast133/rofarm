#include "ProcessController.h"


ro::ProcessController::ProcessController(ro::roAccount account, QObject *parent)
    : QObject(parent), pid_(NULL),
      thread_(new ProcessWork(account))
{
    QObject::connect(thread_.data(), SIGNAL(end()),
                     this, SIGNAL(finished()));
    QObject::connect(thread_.data(), SIGNAL(),
                     this, SIGNAL(err()));
}


ro::ProcessController::ProcessController(HWND pid, roAccount account, QObject *parent)
    : QObject(parent), pid_(pid),      
      thread_(new ProcessWork(pid_, account))
{
    QObject::connect(thread_.data(), SIGNAL(end()),
                     this, SIGNAL(finished()));
    QObject::connect(thread_.data(), SIGNAL(),
                     this, SIGNAL(err()));
}


ro::ProcessController::~ProcessController() {    
}

bool ro::ProcessController::Run() {
    if(thread_->status() != ro::STATUS_WAIT) {
        thread_->start();
        thread_->set_status(ro::STATUS_RUN);
    }
}

bool ro::ProcessController::Stop() {
    if(thread_->status() == ro::STATUS_WAIT) {
        thread_->resume();
    }
    thread_->terminate();
    thread_->set_status(ro::STATUS_STOP);
}

bool ro::ProcessController::Pause() {
    if(thread_->status() == ro::STATUS_WAIT) {
        thread_->resume();
        thread_->set_status(ro::STATUS_RUN);
    } else {
        thread_->suspend();
        thread_->set_status(ro::STATUS_WAIT);
    }
}

