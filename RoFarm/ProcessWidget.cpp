#include "ProcessWidget.h"
#include "wndUtils.h"
#include "procUtils.h"

ro::ProcessWidget::ProcessWidget(HWND pid, roAccount account, QWidget *parent)
    : QWidget(parent), pid_(pid), account_(new roAccount(account)),
      controller_(new ProcessController(pid, account)),
      bttn_pause_(new QPushButton("Pause")),
      bttn_start_(new QPushButton("Start")),
      bttn_stop_(new QPushButton("Stop")),
      bttn_indicate_(new QPushButton("+")),
      lbl_status_(new QLabel(stop_string)),
      lbl_work_(new QLabel("work time(s)")),
      lbl_tp_(new QLabel("tp time(ms)")),
      line_work_(new QLineEdit(QString::number(controller_->process()->work_time_sec()))),
      line_tp_(new QLineEdit(QString::number(controller_->process()->teleport_time_msec()))),
      lhs_layout_(new QHBoxLayout()),
      rhs_layout_(new QHBoxLayout()),
      main_layout_(new QVBoxLayout()),
      //box_(new QGroupBox("id: 0x" + QString::number((long)pid, 16), this))
      box_(new QGroupBox("login: " + account.login, this)),
      block_(false)
{
    initUI();
}


ro::ProcessWidget::ProcessWidget(roAccount account, QWidget *parent)
    : QWidget(parent), account_(new roAccount(account)),
      controller_(new ProcessController(account)),
      bttn_pause_(new QPushButton("Pause")),
      bttn_start_(new QPushButton("Start")),
      bttn_stop_(new QPushButton("Stop")),
      bttn_indicate_(new QPushButton("+")),
      lbl_status_(new QLabel(stop_string)),
      lbl_work_(new QLabel("work time(s)")),
      lbl_tp_(new QLabel("tp time(ms)")),
      line_work_(new QLineEdit(QString::number(controller_->process()->work_time_sec()))),
      line_tp_(new QLineEdit(QString::number(controller_->process()->teleport_time_msec()))),
      lhs_layout_(new QHBoxLayout()),
      rhs_layout_(new QHBoxLayout()),
      main_layout_(new QVBoxLayout()),
      box_(new QGroupBox("login: " + account.login, this)),
      block_(false)
{
    initUI();
}

ro::ProcessWidget::~ProcessWidget() {    
}

void ro::ProcessWidget::set_tp_time(QString mseconds) {
    line_tp_->setText(mseconds);
}

void ro::ProcessWidget::set_work_time(QString seconds) {
    line_work_->setText(seconds);
}

void ro::ProcessWidget::indicate_clicked() {
    if (block_) return;
    int status;
    if (wndUtils::GetWindowStatus(pid_, status)) {
        if (status == SW_SHOWMINIMIZED)
            wndUtils::RestoreWindow(pid_);
        else
            wndUtils::MinimizeWindow(pid_);
    }
}

void ro::ProcessWidget::start_clicked() {
    if (block_) return;
    ProcessWork *const proc = controller_->process();
    proc->set_teleport_time(line_tp_->text().toLong());
    proc->set_work_time(line_work_->text().toLong());
    lbl_status_->setText(start_string);
    disable_input();
    controller_->Run();
}

void ro::ProcessWidget::stop_clicked() {
    if (block_) return;
    ProcessWork *const proc = controller_->process();
    if (proc->status() == ro::STATUS_WAIT)
        bttn_pause_->setText("Pause");

    lbl_status_->setText(stop_string);
    enable_input();
    controller_->Stop();
}

void ro::ProcessWidget::pause_clicked() {
    if (block_) return;
    ProcessWork *const proc = controller_->process();
    if (proc->status() == ro::STATUS_WAIT) {
        lbl_status_->setText(start_string);
        bttn_pause_->setText("Pause");
        controller_->Pause();
    } else {
        lbl_status_->setText(pause_string);
        bttn_pause_->setText("Resume");
        controller_->Pause();
    }
}

void ro::ProcessWidget::change_status() {
    if (block_) return;
    ProcessWork *const proc = controller_->process();
    if (proc->status() == ro::STATUS_FINISH) {
        QSound::play("://finish_signal.wav");
        lbl_status_->setText(finish_string);
        enable_input();
        emit status_changed();
    }
    if (proc->status() == ro::STATUS_ERR) {
        //play error sound here
        lbl_status_->setText(error_string);
        disable_input();
        bttn_indicate_->setDisabled(true);
        bttn_stop_->setDisabled(true);
        bttn_pause_->setDisabled(true);
        block_ = true;
    }
}

void ro::ProcessWidget::initUI()
{
    line_tp_->setMaxLength(5);
    line_work_->setMaxLength(5);
    line_tp_->setMaximumWidth(75);
    line_work_->setMaximumWidth(75);
    bttn_indicate_->setMaximumWidth(25);

    lhs_layout_->addWidget(lbl_status_.data());
    lhs_layout_->addSpacing(10);
    lhs_layout_->addWidget(lbl_tp_.data());
    lhs_layout_->addWidget(line_tp_.data());
    lhs_layout_->addSpacing(10);
    lhs_layout_->addWidget(lbl_work_.data());
    lhs_layout_->addWidget(line_work_.data());
    lhs_layout_->addSpacing(10);
    lhs_layout_->addWidget(bttn_indicate_.data());

    rhs_layout_->addWidget(bttn_start_.data());
    rhs_layout_->addWidget(bttn_pause_.data());
    rhs_layout_->addWidget(bttn_stop_.data());

    main_layout_->addLayout(lhs_layout_.data());
    main_layout_->addLayout(rhs_layout_.data());
    box_->setLayout(main_layout_.data());

    setFixedSize(440, 100);

    QObject::connect(bttn_start_.data(), SIGNAL(clicked()),
                     this, SLOT(start_clicked()));
    QObject::connect(bttn_stop_.data(), SIGNAL(clicked()),
                     this, SLOT(stop_clicked()));
    QObject::connect(bttn_pause_.data(), SIGNAL(clicked()),
                     this, SLOT(pause_clicked()));
    QObject::connect(controller_.data(), SIGNAL(finished()),
                     this, SLOT(change_status()));
    QObject::connect(bttn_indicate_.data(), SIGNAL(clicked()),
                     this, SLOT(indicate_clicked()));
    QObject::connect(controller_.data(), SIGNAL(err()),
                     this, SLOT(change_status()));

    enable_input();
    start_clicked();
}

void ro::ProcessWidget::disable_input() {
    line_tp_->setDisabled(true);
    line_work_->setDisabled(true);
    bttn_start_->setDisabled(true);
    bttn_pause_->setDisabled(false);
}

void ro::ProcessWidget::enable_input() {
    line_tp_->setEnabled(true);
    line_work_->setEnabled(true);
    bttn_start_->setEnabled(true);
    bttn_pause_->setEnabled(false);
}
