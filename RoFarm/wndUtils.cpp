#include "stdafx.h"
#include "wndUtils.h"

namespace wndUtils
{

    BOOL GetWindowStatus(HWND hWnd, int& status)
    {
        WINDOWPLACEMENT wPlst;
        if (GetWindowPlacement(hWnd, &wPlst))
        {
        status = wPlst.showCmd;
        return TRUE;
        }
        return FALSE;
    }

    int MinimizeWindow(HWND hWnd)
    {
        return ShowWindow(hWnd, SW_MINIMIZE);
    }

    int RestoreWindow(HWND hWnd)
    {
        return ShowWindow(hWnd, SW_RESTORE);
    }

	bool getClassNameByHWND(HWND hWnd, tstring& className)
	{
		TCHAR tchWndText[MAX_CLASS_NAME];
		if (!GetClassName(hWnd, tchWndText, MAX_CLASS_NAME))
		{
			DWORD err = GetLastError();
			return false;
		}
		className = tchWndText;
		return true;
	}

	// Structure used to communicate data from and to enumeration procedure
	struct EnumData {
		DWORD dwProcessId;
		HWND hWnd;
	};

	// Application-defined callback for EnumWindows
	BOOL CALLBACK EnumProc(HWND hWnd, LPARAM lParam) {
		// Retrieve storage location for communication data
		EnumData& ed = *(EnumData*)lParam;
		DWORD dwProcessId = 0x0;
		// Query process ID for hWnd
		GetWindowThreadProcessId(hWnd, &dwProcessId);
		// Apply filter - if you want to implement additional restrictions,
		// this is the place to do so.
		if (ed.dwProcessId == dwProcessId) {
			// Found a window matching the process ID
			ed.hWnd = hWnd;
			// Report success
			SetLastError(ERROR_SUCCESS);
			// Stop enumeration
			return FALSE;
		}
		// Continue enumeration
		return TRUE;
	}

	// Main entry
	HWND FindWindowFromProcessId(DWORD dwProcessId) {
		EnumData ed = { dwProcessId };
		if (!EnumWindows(EnumProc, (LPARAM)&ed) &&
			(GetLastError() == ERROR_SUCCESS)) {
			return ed.hWnd;
		}
		return NULL;
	}

	// Helper method for convenience
	HWND FindWindowFromProcess(HANDLE hProcess) {
        return FindWindowFromProcessId(GetProcessId(hProcess));
    }
}
