#ifndef PROCUTILS_H
#define PROCUTILS_H

#include <vector>
#include <TlHelp32.h>
#include "stdafx.h"
namespace procUtils
{
	//TH32CS_SNAPPROCESS
	void getProcIdsByName(const tstring& procName, std::vector<DWORD>& idProcs, DWORD flags = TH32CS_SNAPPROCESS);
}

#endif // PROCUTILS_H
