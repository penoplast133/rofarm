#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include "ProcessWidget.h"
#include <QToolBar>

namespace ro {

class ProcessManager : public QWidget
{
    Q_OBJECT
public:
    ProcessManager(QWidget *parent = 0);
    ProcessManager(const QList<HWND>& hwnd_list, QWidget *parent = 0);
    virtual ~ProcessManager();

public slots:
    void mass_stop();
    void mass_run();
    void mass_finish();

private:
    void disable_input();
    void enable_input();
    void init_mass_ctrl_panel();

private:
    QList<HWND> hwnd_list_;
    QList<ProcessWidget*> wgt_ptr_list_;
    QSharedPointer<QVBoxLayout> main_layout_;
    QLineEdit *tp_le;
    QLineEdit *work_le;
    QPushButton *run_bttn;
    QPushButton *stop_bttn;
};
}

#endif // PROCESSMANAGER_H
