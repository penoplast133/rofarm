#-------------------------------------------------
#
# Project created by QtCreator 2014-07-18T18:17:21
#
#-------------------------------------------------

QT       += core gui multimedia xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RoFarm
TEMPLATE = app
DEFINES += _UNICODE


SOURCES += main.cpp\
    ProcessWidget.cpp \
    ProcessWork.cpp \
    ProcessController.cpp \
    ProcessManager.cpp \
    procUtils.cpp \
    wndUtils.cpp \
    roUtils.cpp

HEADERS  += \
    ProcessWidget.h \
    ProcessWork.h \
    ProcessController.h \
    ProcessManager.h \
    procUtils.h \
    wndUtils.h \
    stdafx.h \
    roUtils.h

RC_FILE = myapp.rc

RESOURCES += \
    res.qrc
