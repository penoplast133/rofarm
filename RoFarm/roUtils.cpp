#include "roUtils.h"

QList<ro::roCommand> ro::command_list(const QString cmd_file_path)
{
    QFile file(cmd_file_path);
    QList<ro::roCommand> list;

    if (!file.exists()) {
        qDebug() << "Command file not exist.";
        return list;
    }

    QDomDocument doc;

    file.open(QIODevice::ReadOnly);
    doc.setContent(&file);
    file.close();

    QDomElement root = doc.firstChildElement("Commands");
    QDomElement el = root.firstChildElement("Command");
    QDomElement data_el;
    QDomNode entry;
    ro::roCommand cmd;

    for (; !el.isNull(); el = el.nextSiblingElement("Command")) {
        entry = el.firstChild();
        while (!entry.isNull()) {
            data_el = entry.toElement();
            if (data_el.tagName() == "cmd")
                cmd.cmd = data_el.text();
            if (data_el.tagName() == "delay") {
                cmd.delay = data_el.text().toInt();
            }
            if (data_el.tagName() == "repeat") {
                cmd.repeat = data_el.text().toInt();
                list.append(cmd);
            }

            entry = entry.nextSibling();
        }
    }

    return list;
}


QList<ro::roAccount> ro::account_list()
{
    QFile file(accounts_file);
    QList<ro::roAccount> list;

    if (!file.exists()) {
        qDebug() << "Login file not exist.";
        return list;
    }

    QDomDocument doc;

    file.open(QIODevice::ReadOnly);
    doc.setContent(&file);
    file.close();

    QDomElement root = doc.firstChildElement("Accounts");
    QDomElement el = root.firstChildElement("Account");
    QDomElement data_el;
    QDomNode entry;
    QString login, password;

    for (; !el.isNull(); el = el.nextSiblingElement("Account")) {
        entry = el.firstChild();
        while (!entry.isNull()) {
            data_el = entry.toElement();
            if (data_el.tagName() == "login")
                login = data_el.text();
            if (data_el.tagName() == "password") {
                password = data_el.text();
                list.append(ro::roAccount(login, password));
            }
            entry = entry.nextSibling();
        }
    }

    return list;
}


void ro::add_account(ro::roAccount account)
{
    QDomDocument doc;
    QDomProcessingInstruction instr = doc.createProcessingInstruction(
                "xml", "version='1.0' encoding='UTF-8'");
    QDomElement root = doc.createElement("Accounts");
    QDomElement element = doc.createElement("Account");
    QDomElement login_tag = doc.createElement("login");
    QDomText login = doc.createTextNode(account.login);
    QDomElement password_tag = doc.createElement("password");
    QDomText password = doc.createTextNode(account.password);

    QFile file(accounts_file);
    if (!file.exists()) {
        doc.appendChild(instr);
    } else {
        file.open(QIODevice::ReadOnly);
        doc.setContent(&file);
        file.close();
        root = doc.documentElement();
    }

    doc.appendChild(root);
    root.appendChild(element);
    element.appendChild(login_tag);
    login_tag.appendChild(login);
    element.appendChild(password_tag);
    password_tag.appendChild(password);

    QString xml_out = doc.toString();
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream outstream(&file);
    outstream << xml_out;
    file.close();
}

void ro::SendSeqMsg(HWND hWnd, ro::roCommand& cmd) {
    QStringList cmd_list = cmd.cmd.split(" ", QString::SkipEmptyParts);    
    int key = 0;

    for (int i = 0; i < cmd_list.size(); ++i) {
        if (cmd_list[i][0] == '\"') {
            for (int j = 1; j < cmd_list[i].size()-1; ++j) {
                PostMessage(hWnd, WM_KEYDOWN, cmd_list[i][j].unicode(), 0);
                Sleep(cmd.delay);
            }
        } else {
        if (cmd_list[i] == "ENTER")     {key = VK_RETURN;   goto quit;}
        if (cmd_list[i] == "SPACE")     {key = VK_SPACE;    goto quit;}
        if (cmd_list[i] == "ESCAPE")    {key = VK_ESCAPE;   goto quit;}
        if (cmd_list[i] == "TAB")       {key = VK_TAB;      goto quit;}
        if (cmd_list[i] == "BACKSPACE") {key = VK_BACK;     goto quit;}
        if (cmd_list[i] == "CAPSLOCK")  {key = VK_CAPITAL;  goto quit;}
        if (cmd_list[i] == "UP")        {key = VK_UP;       goto quit;}
        if (cmd_list[i] == "DOWN")      {key = VK_DOWN;     goto quit;}
        if (cmd_list[i] == "LEFT")      {key = VK_LEFT;     goto quit;}
        if (cmd_list[i] == "RIGHT")     {key = VK_RIGHT;    goto quit;}
        if (cmd_list[i] == "SNAPSHOT")  {key = VK_SNAPSHOT; goto quit;}
        if (cmd_list[i] == "F1")  {key = VK_F1; goto quit;}
        if (cmd_list[i] == "F2")  {key = VK_F2; goto quit;}
        if (cmd_list[i] == "F3")  {key = VK_F3; goto quit;}
        if (cmd_list[i] == "F4")  {key = VK_F4; goto quit;}
        if (cmd_list[i] == "F5")  {key = VK_F5; goto quit;}
        if (cmd_list[i] == "F6")  {key = VK_F6; goto quit;}
        if (cmd_list[i] == "F7")  {key = VK_F7; goto quit;}
        if (cmd_list[i] == "F8")  {key = VK_F8; goto quit;}
        if (cmd_list[i] == "F9")  {key = VK_F9; goto quit;}
        if (cmd_list[i] == "F10") {key = VK_F10; goto quit;}
        if (cmd_list[i] == "F11") {key = VK_F11; goto quit;}
        if (cmd_list[i] == "F12") {key = VK_F12; goto quit;}
        key = 0;
quit:
        if (key != 0) {
            PostMessage(hWnd, WM_KEYDOWN, key, 0);
            Sleep(cmd.delay);
        }
        }
    }
}

void ro::SendMsg(HWND hWnd, QString cmd, int delay) {
    int key = 0;    
    if (cmd[0] == '\"') {
        for (int i = 1; i < cmd.size()-1; ++i) {
            PostMessage(hWnd, WM_KEYDOWN, cmd[i].unicode(), 0);
            Sleep(delay);
        }
    } else {
        if (cmd == "ENTER")     {key = VK_RETURN;   goto quit;}
        if (cmd == "SPACE")     {key = VK_SPACE;    goto quit;}
        if (cmd == "ESCAPE")    {key = VK_ESCAPE;   goto quit;}
        if (cmd == "TAB")       {key = VK_TAB;      goto quit;}
        if (cmd == "CAPSLOCK")  {key = VK_CAPITAL;  goto quit;}
        if (cmd == "BACKSPACE") {key = VK_BACK;     goto quit;}
        if (cmd == "UP")        {key = VK_UP;       goto quit;}
        if (cmd == "DOWN")      {key = VK_DOWN;     goto quit;}
        if (cmd == "LEFT")      {key = VK_LEFT;     goto quit;}
        if (cmd == "RIGHT")     {key = VK_RIGHT;    goto quit;}
        if (cmd == "SNAPSHOT")  {key = VK_SNAPSHOT; goto quit;}
        if (cmd == "F1")  {key = VK_F1; goto quit;}
        if (cmd == "F2")  {key = VK_F2; goto quit;}
        if (cmd == "F3")  {key = VK_F3; goto quit;}
        if (cmd == "F4")  {key = VK_F4; goto quit;}
        if (cmd == "F5")  {key = VK_F5; goto quit;}
        if (cmd == "F6")  {key = VK_F6; goto quit;}
        if (cmd == "F7")  {key = VK_F7; goto quit;}
        if (cmd == "F8")  {key = VK_F8; goto quit;}
        if (cmd == "F9")  {key = VK_F9; goto quit;}
        if (cmd == "F10") {key = VK_F10; goto quit;}
        if (cmd == "F11") {key = VK_F11; goto quit;}
        if (cmd == "F12") {key = VK_F12; goto quit;}
        key = 0;
quit:
        if (key != 0) {
            PostMessage(hWnd, WM_KEYDOWN, key, 0);
            Sleep(delay);
        }
    }
}
