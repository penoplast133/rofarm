#include "ProcessWork.h"


ro::ProcessWork::ProcessWork(HWND pid, roAccount account)
    : pid_(pid), account_(new ro::roAccount(account)),
      status_(ro::STATUS_STOP),
      teleport_time_mseconds_(1000),
      work_time_seconds_(1),      
      pause_(false),
      login_(true)
{
    QObject::connect(this, SIGNAL(finished()),
                     this, SLOT(change_status_finish()));
}

ro::ProcessWork::ProcessWork(roAccount account)
    : account_(new ro::roAccount(account)),
      status_(ro::STATUS_STOP),
      teleport_time_mseconds_(1000),
      work_time_seconds_(1),
      pause_(false),      
      login_(true)
{
    QObject::connect(this, SIGNAL(finished()),
                     this, SLOT(change_status_finish()));
}

ro::ProcessWork::~ProcessWork() {
    ///! Terminate window
    PostMessage(pid_, WM_CLOSE, 0, 0);
}

void ro::ProcessWork::run() {
    if (login_) {
        //run_client();
        logining();
        login_ = false;
    }

    int work_time_ms = work_time_seconds_ * 1000;
    int pm_num = static_cast<int>(work_time_ms / teleport_time_mseconds_);

    for (int i = 0; i < pm_num; ++i) {
        ///! Place for thread suspend
        sync_.lock();
        if (pause_)
            pauseCond_.wait(&sync_);
        sync_.unlock();

        if (!IsWndExist()) {
            set_status(ro::STATUS_ERR);
            emit end();
            return;
        }

        PostMessage(pid_, WM_KEYDOWN, VK_F2, 0);
        Sleep(teleport_time_mseconds_);
    }

    set_status(ro::STATUS_FINISH);
    emit end();
}

void ro::ProcessWork::suspend() {
    sync_.lock();
    pause_ = true;
    sync_.unlock();
}

void ro::ProcessWork::resume() {
    sync_.lock();
    pause_ = false;
    sync_.unlock();
    pauseCond_.wakeAll();
}

void ro::ProcessWork::change_status_finish() {
    status_ = ro::STATUS_FINISH;
}

void ro::ProcessWork::run_client() {
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    WCHAR file_path[] = L"C:\\IRR\\ro.bin";
    WCHAR curr_dir_file[] = L"ro.bin";

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );

    if( !CreateProcess( file_path,   // No module name (use command line)
                        NULL,        // Command line
                        NULL,           // Process handle not inheritable
                        NULL,           // Thread handle not inheritable
                        FALSE,          // Set handle inheritance to FALSE
                        0,              // No creation flags
                        NULL,           // Use parent's environment block
                        NULL,           // Use parent's starting directory
                        &si,            // Pointer to STARTUPINFO structure
                        &pi )           // Pointer to PROCESS_INFORMATION structure
            ) {
        //MessageBox with error
    }

    pid_ = wndUtils::FindWindowFromProcessId(pi.dwProcessId);
    pid_ = wndUtils::FindWindowFromProcess(pi.hProcess);
    Sleep(6000);
    PostMessage(pid_, WM_KEYDOWN, VK_RETURN, 0);    
}

void ro::ProcessWork::logining() {    
    ctrl(auth_file);
}

void ro::ProcessWork::ctrl(const QString xml_file) {
    QList<ro::roCommand> cmd_list = ro::command_list(xml_file);
    QStringList sep_cmd_list;

    for (int i = 0; i < cmd_list.size(); ++i) {
        sep_cmd_list = cmd_list[i].cmd.split(" ", QString::SkipEmptyParts);        
        for (int j = 0; j < sep_cmd_list.size(); ++j) {
            if (sep_cmd_list[j] == "LOGIN")
                ro::SendMsg(pid_, account_->login_cmd.cmd, account_->login_cmd.delay);
            if (sep_cmd_list[j] == "PASSWORD")
                ro::SendMsg(pid_, account_->password_cmd.cmd, account_->password_cmd.delay);
            else {
                ro::SendMsg(pid_, sep_cmd_list[j], cmd_list[i].delay);
            }
        }
    }
}


