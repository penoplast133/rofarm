#ifndef PROCESSWORK_H
#define PROCESSWORK_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <windows.h>
#include <wndUtils.h>
#include "roUtils.h"
#include <QDebug>

namespace ro {

enum ProcessStatus {
    STATUS_STOP = 0,
    STATUS_RUN,
    STATUS_WAIT,
    STATUS_FINISH,
    STATUS_ERR
};

class ProcessWork : public QThread
{
    Q_OBJECT

public:
    explicit ProcessWork(HWND pid, roAccount account);
    ProcessWork(roAccount account);
    virtual ~ProcessWork();

    bool IsWndExist() { return IsWindow(pid_); }
    inline ProcessStatus status() { return status_; }
    inline DWORD teleport_time_msec() { return teleport_time_mseconds_; }
    inline DWORD work_time_sec() { return work_time_seconds_; }
    inline HWND pid() { return pid_; }
    inline void set_teleport_time(DWORD mseconds) { teleport_time_mseconds_ = mseconds; }
    inline void set_work_time(DWORD seconds) { work_time_seconds_ = seconds; }
    inline void set_status(ProcessStatus status) { status_ = status; }

    void run();
    void suspend();
    void resume();

public slots:
    void change_status_finish();

signals:
    void end();
    void err();

private:
    void run_client();
    void logining();
    void logout();
    void ctrl(QString xml_file);

private:
    HWND pid_;
    DWORD teleport_time_mseconds_;
    DWORD work_time_seconds_;
    ProcessStatus status_;
    QSharedPointer<ro::roAccount> account_;
    bool login_;
    bool logout_;

    QMutex sync_;
    QWaitCondition pauseCond_;
    bool pause_;
};
}

#endif // PROCESSWORK_H
