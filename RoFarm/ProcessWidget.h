#ifndef PROCESSWIDGET_H
#define PROCESSWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QSharedPointer>
#include <QSound>
#include "ProcessController.h"

namespace ro {

class ProcessWidget : public QWidget
{
    Q_OBJECT
public:
    ProcessWidget(roAccount account, QWidget *parent = 0);
    ProcessWidget(HWND pid, roAccount account, QWidget *parent = 0);
    void set_tp_time(QString mseconds);
    void set_work_time(QString seconds);
    bool is_blocked() { return block_; }
    virtual ~ProcessWidget();

signals:
    void status_changed();

public slots:
    void indicate_clicked();
    void start_clicked();
    void stop_clicked();
    void pause_clicked();
    void change_status();

private:
    void initUI();
    void disable_input();
    void enable_input();

private:
    const QString stop_string = "status: <font color=\"blue\">STOPED</font>";
    const QString start_string = "status: <font color=\"green\">RUNNING</font>";
    const QString pause_string = "status: <font color=\"orange\">PAUSED</font>";
    const QString finish_string = "status: <font color=\"blue\">FINISHED</font>";
    const QString error_string = "status: <font color=\"red\"><b>ERROR</b></font>";
    bool block_;
    HWND pid_;    
    QSharedPointer<roAccount> account_;
    QSharedPointer<QGroupBox> box_;
    QSharedPointer<QVBoxLayout> main_layout_;
    QSharedPointer<QHBoxLayout> lhs_layout_;
    QSharedPointer<QHBoxLayout> rhs_layout_;
    QSharedPointer<ProcessController> controller_;
    QSharedPointer<QPushButton> bttn_start_;
    QSharedPointer<QPushButton> bttn_stop_;
    QSharedPointer<QPushButton> bttn_pause_;
    QSharedPointer<QPushButton> bttn_indicate_;
    QSharedPointer<QLabel> lbl_tp_;
    QSharedPointer<QLabel> lbl_work_;
    QSharedPointer<QLabel> lbl_status_;
    QSharedPointer<QLineEdit> line_tp_;
    QSharedPointer<QLineEdit> line_work_;
};
}

#endif // PROCESSWIDGET_H
